import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../../services/employee.service';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css'],
  providers: [EmployeeService]
})
export class EmployeeComponent implements OnInit {
  employees: any = [];
  employeeForm: FormGroup;
  status = 'Add';

  constructor(private employeeService: EmployeeService, private fb: FormBuilder) { }

  ngOnInit() {

    this.employeeForm = this.fb.group({
      '_id': new FormControl(null),
      'first_name': new FormControl(null, Validators.required),
      'last_name': new FormControl(null, Validators.required),
      'phone': new FormControl(null, Validators.required)
    });

    this.getEmployees();

  }

  deleteEmployee(id: any) {
    this.employeeService.deleteEmployee(id).subscribe(response => {
      this.getEmployees();
    })
  }

  addEmployee() {
    this.employeeService.addEmployee(this.employeeForm.value).subscribe(employee => {
      this.getEmployees();
      this.employeeForm.reset();
    })
  }
  getEmployees() {
    this.employeeService.getEmployees().subscribe((list: any) => {
      this.employees = list;
    });
  }

  editEmployee(index) {
    this.employeeForm.patchValue(this.employees[index]);
    this.status = 'Update';
  }

  updateEmployee() {
    this.employeeService.updateEmployee(this.employeeForm.value).subscribe(employee => {
      this.getEmployees();
      this.employeeForm.reset();
      this.status = 'Add';
    })
  }
}
